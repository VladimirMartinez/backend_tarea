package com.mitocode.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitocode.dao.IProductoDAO;
import com.mitocode.model.Producto;
import com.mitocode.service.IProductoService;

@Service
public class ProductoServiceImpl implements IProductoService {
	
	
	@Autowired
	private IProductoDAO dao;

	@Override
	public void registrar(Producto producto) {
		dao.save(producto);
		
	}

	@Override
	public void modificar(Producto producto) {
		dao.save(producto);
		
	}

	@Override
	public void eliminar(int idProducto) {
		dao.delete(idProducto);
		
	}

	@Override
	public List<Producto> listar() {
		// TODO Auto-generated method stub
		return dao.findAll();
	}

	@Override
	public Producto listarId(int idProducto) {
		// TODO Auto-generated method stub
		return dao.findOne(idProducto);
	}

}
