package com.mitocode.service;

import java.util.List;

import com.mitocode.model.Producto;

public interface IProductoService {
	
	void registrar (Producto producto);
	
	void modificar (Producto producto);
	
	void eliminar(int idProducto);
	
	List<Producto>listar();
	
	Producto listarId(int idProducto);

}
