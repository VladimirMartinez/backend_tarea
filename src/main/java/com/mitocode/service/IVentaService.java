package com.mitocode.service;

import java.util.List;

import com.mitocode.model.Venta;

public interface IVentaService {
	
	Venta registrar (Venta venta);
	
	void modificar(Venta venta);
	
	void eliminar(int idVenta);
	
	List<Venta>listar();
	
	Venta listarId(int idVenta);

}
