package com.mitocode.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.model.Persona;
import com.mitocode.service.IPersonaService;

@RestController
@RequestMapping("/personas")
public class PersonaController {
	
	@Autowired
	private IPersonaService service;
	
	
	@GetMapping(value="/listar", produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Persona>> listar(){
		
		List<Persona> persona= new ArrayList<>();
		
		try {
			persona = service.listar();
		} catch (Exception e) {
			// TODO: handle exception
			
			return new ResponseEntity<List<Persona>>(persona, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<List<Persona>>(persona, HttpStatus.OK);
	}
	
	@GetMapping(value="/listar/{id}", produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Persona>listarId(@PathVariable("id") Integer idPersona){
		Persona per= new Persona();
		
		try {
			per= service.listarId(idPersona);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<Persona>(per, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<Persona>(per, HttpStatus.OK);
	}
	
	@PostMapping(value="/registrar", consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Persona> registrar(@RequestBody Persona persona){
		
		Persona per=new Persona();
		
		try {
			per=service.registrar(persona);
		} catch (Exception e) {
			// TODO: handle exception
			
			return new ResponseEntity<Persona>(per, HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
		return new ResponseEntity<Persona>(per, HttpStatus.OK);
	}
	
	@PutMapping(value="/actualizar", consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer>modificar(@RequestBody Persona persona){
		int resultado = 0;
		
		try {
			service.modificar(persona);
			resultado=1;
		} catch (Exception e) {
			// TODO: handle exception
			
			return new ResponseEntity<Integer>(resultado, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<Integer>(resultado, HttpStatus.OK);
		
	}
	
	@GetMapping(value="/eliminar/{id}", produces=MediaType.APPLICATION_JSON_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> eliminar(@PathVariable("id") Integer idPersona){
		int resultado =0;
		
		try {
			service.eliminar(idPersona);
			resultado=1;
		} catch (Exception e) {
			// TODO: handle exception
			
			return new ResponseEntity<Integer>(resultado, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<Integer>(resultado, HttpStatus.OK);
	}

}
